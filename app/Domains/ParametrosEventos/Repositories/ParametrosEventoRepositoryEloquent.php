<?php


namespace App\Domains\ParametrosEventos\Repositories;


use App\Domains\ParametrosEventos\ParametrosEvento;
use App\Domains\ParametrosEventos\Presenters\ParametrosEventoPresenter;
use Prettus\Repository\Eloquent\BaseRepository;

class ParametrosEventoRepositoryEloquent extends BaseRepository implements ParametrosEventoRepositoryInterface
{


    public function model()
    {
        return ParametrosEvento::class;
    }


    public function presenter()
    {
        // ParametrosEventoPresenter::class;
    }

}