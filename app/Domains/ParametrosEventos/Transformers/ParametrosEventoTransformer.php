<?php

namespace App\Domains\ParametrosEventos\Transformers;


use App\Domains\ParametrosEventos\ParametrosEvento;
use League\Fractal\TransformerAbstract;

class ParametrosEventoTransformer extends TransformerAbstract
{

    /**
     * @param ParametrosEvento $parametrosEvento
     * @return array
     */
    public function transform(ParametrosEvento $parametrosEvento)
    {
        return  [
        'peve_codigo' => $parametrosEvento->peve_codigo,
        'peve_even_codigo' => $parametrosEvento->peve_even_codigo,
        'peve_nomeparametro' => $parametrosEvento->peve_nomeparametro,
        'peve_valorparametro' => $parametrosEvento->peve_valorparametro
        ];
    }

}