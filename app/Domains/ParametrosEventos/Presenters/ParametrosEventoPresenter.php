<?php


namespace App\Domains\ParametrosEventos\Presenters;


use App\Domains\ParametrosEventos\Transformers\ParametrosEventoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ParametrosEventoPresenter extends FractalPresenter
{

    protected $resourceKeyItem = 'parametrosevento';
    protected $resourceKeyCollection = 'parametrosevento';

    public function getTransformer()
    {
        return new ParametrosEventoTransformer();
    }
    
}