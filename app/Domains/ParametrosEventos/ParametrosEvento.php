<?php

namespace App\Domains\ParametrosEventos;


use Illuminate\Database\Eloquent\Model;

class ParametrosEvento extends Model
{
    protected $table = "parametrosevento";

    protected $fillable = [
        'peve_codigo',
        'peve_even_codigo',
        'peve_nomeparametro',
        'peve_valorparametro'
    ];
}