<?php


namespace App\Domains\ComandosEventos;


use Illuminate\Database\Eloquent\Model;

class ComandosEvento extends Model
{

    protected $table = "comandosevento";

    protected $fillable = [
        'ceve_codigo',
        'ceve_even_codigo',
        'ceve_nomecomando',
        'ceve_valorcomando'
    ];

}