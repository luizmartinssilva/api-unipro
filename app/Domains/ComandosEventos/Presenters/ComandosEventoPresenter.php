<?php

namespace App\Domains\ComandosEventos\Presenters;


use App\Domains\ComandosEventos\Transformers\ComandosEventoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ComandosEventoPresenter extends FractalPresenter
{
    protected $resourceKeyItem = 'comandosevento';
    protected $resourceKeyCollection = 'comandosevento';

    public function getTransformer()
    {
        return new ComandosEventoTransformer();
    }

}