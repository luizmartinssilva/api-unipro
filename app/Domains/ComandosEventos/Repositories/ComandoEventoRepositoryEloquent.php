<?php

namespace App\Domains\ComandosEventos\Repositories;


use App\Domains\ComandosEventos\ComandosEvento;
use Prettus\Repository\Eloquent\BaseRepository;

class ComandoEventoRepositoryEloquent extends  BaseRepository implements ComandoEventoRepositoryInterface
{

    public function model()
    {
        return ComandosEvento::class;
    }


    public function presenter()
    {
        //return parent::presenter(); // TODO: Change the autogenerated stub
    }

}