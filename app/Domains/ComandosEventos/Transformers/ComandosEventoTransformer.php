<?php

namespace App\Domains\ComandosEventos\Transformers;

use App\Domains\ComandosEventos\ComandosEvento;
use League\Fractal\TransformerAbstract;

class ComandosEventoTransformer extends  TransformerAbstract
{
    public function transform(ComandosEvento $comandosEvento)
    {
        return  [
            'ceve_codigo' => $comandosEvento->ceve_codigo,
            'ceve_even_codigo' => $comandosEvento->ceve_even_codigo,
            'ceve_nomecomando' => $comandosEvento->ceve_nomecomando,
            'ceve_valorcomando' => $comandosEvento->ceve_valorcomando
        ];
    }

}