<?php


namespace App\Domains\Eventos;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{

    protected $table = 'eventos';

    protected $fillable = [
        'even_codigo',
        'even_datahora',
        'even_nomerelatorio',
        'even_usua_codigo'
    ];

}