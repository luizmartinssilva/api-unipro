<?php

namespace App\Domains\Eventos\Transformers;


use App\Domains\Eventos\Evento;
use League\Fractal\TransformerAbstract;

class EventoTransformer extends TransformerAbstract
{
    public function transform(Evento $evento)
    {
        return  [
            'even_codigo' => $evento->even_codigo,
            'even_datahora' => $evento->even_datahora,
            'even_nomerelatorio' => $evento->even_nomerelatorio,
            'even_usua_codigo' => $evento->even_usua_codigo
        ];
    }

}