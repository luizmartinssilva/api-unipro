<?php

namespace App\Domains\Eventos\Presenters;


use App\Domains\Eventos\Transformers\EventoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class EventoPresenter extends FractalPresenter
{

    protected $resourceKeyItem = 'eventos';
    protected $resourceKeyCollection = 'eventos';

    public function getTransformer()
    {
        return new EventoTransformer();
    }

}