<?php

namespace App\Applications\Api\V1\Http\Controllers\ParametrosEventos;


use App\Applications\Api\Traits\Rest\ResponseHelpers;
use App\Core\Http\Controllers\Controller;
use App\Domains\ParametrosEventos\Repositories\ParametrosEventoRepositoryEloquent;

class ParametrosEventoController extends Controller
{
    use ResponseHelpers;


    private $repository;


    /**
     * ParametrosEventoController constructor.
     * @param ParametrosEventoRepositoryEloquent $repository
     */
    public function __construct( ParametrosEventoRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }


    public function index()
    {
        return $this->ApiResponse($this->repository->all());
    }
}