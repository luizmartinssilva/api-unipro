<?php


namespace App\Applications\Api\V1\Http\Controllers\ComandosEventos;


use App\Applications\Api\Traits\Rest\ResponseHelpers;
use App\Core\Http\Controllers\Controller;
use App\Domains\ComandosEventos\Repositories\ComandoEventoRepositoryEloquent;

class ComandosEventoController extends Controller
{

    use ResponseHelpers;


    /**
     * @var ComandoEventoRepositoryEloquent
     */
    private $comandoEventoRepositoryEloquent;

    /**
     * ComandosEventoController constructor.
     * @param ComandoEventoRepositoryEloquent $comandoEventoRepositoryEloquent
     */
    public function __construct( ComandoEventoRepositoryEloquent $comandoEventoRepositoryEloquent)
    {
        $this->comandoEventoRepositoryEloquent = $comandoEventoRepositoryEloquent;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        return $this->ApiResponse($this->comandoEventoRepositoryEloquent->all());
    }

}