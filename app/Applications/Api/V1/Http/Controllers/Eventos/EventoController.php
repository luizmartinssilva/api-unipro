<?php


namespace App\Applications\Api\V1\Http\Controllers\Eventos;


use App\Applications\Api\Traits\Rest\ResponseHelpers;
use App\Core\Http\Controllers\Controller;
use App\Domains\Eventos\Repositories\EventoRepositoryEloquent;

class EventoController extends Controller
{

    use ResponseHelpers;

    /**
     * @var EventoRepositoryEloquent
     */
    private $eventoRepositoryEloquent;

    /**
     * EventoController constructor.
     * @param EventoRepositoryEloquent $eventoRepositoryEloquent
     */
    public function __construct( EventoRepositoryEloquent $eventoRepositoryEloquent)
    {
        $this->eventoRepositoryEloquent = $eventoRepositoryEloquent;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){

        return $this->ApiResponse($this->eventoRepositoryEloquent->all());

    }


}